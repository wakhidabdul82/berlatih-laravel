<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('homepage.register');
    }

    public function user(Request $pengguna){
        $namadepan = $pengguna['fname'];
        $namabelakang = $pengguna['lname'];

        return view('homepage.welcome', compact('namadepan', 'namabelakang'));
    }
}

